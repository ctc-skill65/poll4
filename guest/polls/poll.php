<?php
require_once __DIR__ . '/../../boot.php';

$poll_id = get('id');
$page_path = "/guest/polls/poll.php?id={$poll_id}";

if ($_POST) {
    $action = DB::insert('actions', [
        'poll_id' => $poll_id,
        'action_time' => date(DATE_SQL)
    ]); 

    if (!$action) {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถส่งแบบสำรวจได้");
        redirect($page_path);
    }

    $action_id = DB::insert_id();

    $data = [];
    $q = post('q');
    foreach ($q as $key => $value) {
        $data[] = [
            'action_id' => $action_id,
            'q_id' => $key,
            'ans_id' => $value
        ];
    }

    $action_items = DB::insert_multi('action_items', $data);
    if ($action_items) {
        setAlert('success', "ส่งแบบสำรวจสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถส่งคำตอบได้");
    }

    redirect($page_path);
}

$data = DB::row("SELECT * FROM `polls` 
INNER JOIN `users` ON `users`.`user_id`=`polls`.`poll_type_id`
LEFT JOIN  `poll_types` ON `poll_types`.`poll_type_id`=`polls`.`poll_type_id`
WHERE `polls`.`poll_id`='{$poll_id}'");

$items = DB::result("SELECT * FROM `questions` WHERE `poll_id`='{$poll_id}'");
foreach ($items as &$item) {
    $item['anss'] = DB::result("SELECT * FROM `answers` WHERE `q_id`='{$item['q_id']}'");
    unset($item);
}

ob_start();
?>
<?= showAlert() ?>

<h2><?= $data['poll_name'] ?></h2>
<p>
    ประเภทแบบสำรวจ: <?= $data['poll_type_name'] ?>
    <br>
    สร้างโดย: <?= $data['firstname'] . ' ' . $data['lastname'] ?>
</p>

<form method="post">
    <?php foreach ($items as $item) : ?>
        <br>
        <h4><?= $item['q_name'] ?></h4>
        <ul>
            <?php foreach ($item['anss'] as $ans) : ?>
                <input type="radio" name="q[<?= $item['q_id'] ?>]" id="ans<?= $ans['ans_id'] ?>" value="<?= $ans['ans_id'] ?>" required>
                <label for="ans<?= $ans['ans_id'] ?>"><?= $ans['ans_name'] ?></label>
                <br>
            <?php endforeach; ?>
        </ul>
    <?php endforeach; ?>

    <button type="submit">ส่งแบบสำรวจ</button>
</form>
<?php
$layout_page = ob_get_clean();
$page_name = 'แบบสำรวจ';
require ROOT . '/guest/layout.php';
