<?php
require_once __DIR__ . '/../../boot.php';

$page_path = "/guest/polls/list.php";

$type = get('type');

if ($type) {
    $sql = "SELECT * FROM `polls` 
    INNER JOIN `users` ON `users`.`user_id`=`polls`.`poll_type_id`
    LEFT JOIN  `poll_types` ON `poll_types`.`poll_type_id`=`polls`.`poll_type_id`
    WHERE `polls`.`poll_type_id`='{$type}'";
} else {
    $sql = "SELECT * FROM `polls` 
    INNER JOIN `users` ON `users`.`user_id`=`polls`.`poll_type_id`
    LEFT JOIN  `poll_types` ON `poll_types`.`poll_type_id`=`polls`.`poll_type_id`";
}


$items = DB::result($sql);
$poll_types = DB::result("SELECT * FROM `poll_types`");
ob_start();
?>
<?= showAlert() ?>
<form method="get">
    <label for="type">เลือกประเภทแบบสำรวจ</label>
    <select name="type" id="type" required>
        <option value="" selected disabled>---- เลือก ----</option>
        <?php foreach ($poll_types as $item) : ?>
            <option value="<?= $item['poll_type_id'] ?>" 
            <?= $item['poll_type_id'] === $type ? 'selected' : null?>
            >
                <?= $item['poll_type_name'] ?>
            </option>
        <?php endforeach; ?>
    </select>

    <button type="submit">ค้นหา</button>
</form>

<h3>รายการแบบสำรวจ</h3>
<table>
    <thead>
        <tr>
            <th>รหัส</th>
            <th>แบบสำรวจ</th>
            <th>ประเภทแบบสำรวจ</th>
            <th>ผู้สร้างแบบสำรวจ</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['poll_id'] ?></td>
                <td><?= $item['poll_name'] ?></td>
                <td><?= $item['poll_type_name'] ?></td>
                <td><?= $item['firstname'] . ' ' . $item['lastname'] ?></td>
                <td>
                    <a href="<?= url("/guest/polls/poll.php?id={$item['poll_id']}") ?>" target="_blank" rel="noopener noreferrer">เปิดแบบสำรวจ</a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'รายการแบบสำรวจ';
require ROOT . '/guest/layout.php';
