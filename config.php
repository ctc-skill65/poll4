<?php

return [
    'app_name' => 'ระบบแบบสำรวจออนไลน์ (Poll)',
    'site_url' => 'http://skill65.local/poll4',

    'db_host' => 'localhost',
    'db_user' => 'root',
    'db_password' => 'root',
    'db_name' => 'skill65_poll4',
    'db_charset' => 'utf8mb4'
];
